#!/usr/bin/env node
// 注释意思：告诉系统执行到此文件用node去执行
import {Command } from 'commander'
import fs from 'node:fs'
import path from 'node:path';
import inquirer from 'inquirer'
import { downTemplate } from './utils.js';
const program = new Command();
// 拿到package.json的版本
const jsonStr = fs.readFileSync('./package.json', 'utf-8')
const jsonObj = JSON.parse(jsonStr)
program.version(jsonObj.version)

program.command('create <projectName>')
.description('下载模板代码')
.alias('c')
.action((res) => {
    console.log('projectName',res);
    inquirer.prompt([
        {
            type: 'input',
            message: '请输入项目名称',
            default: res,
            name: 'projectName'
        },
        {
            type: 'confirm',
            name: 'isTs',
            message: '是否选择typeScript模板'
        },
        {
            type: 'list',
            name: 'frame',
            message: '选择一种框架',
            choices: [
                // "vue2",
                "Vue3",
                // "react"
            ]
        }
    ]).then(res => {
        console.log('res',res);
        if(fs.existsSync(res.projectName)) {
            console.log('目录已存在');
            return
        }
        if(res.isTs) {
            downTemplate('dev', res.projectName)
        } else{
            downTemplate('js', res.projectName)
        }
    })
})

program.parse();