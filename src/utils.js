import download from 'download-git-repo'
import ora from 'ora'

export const downTemplate = (branch, name) => {
    const spinner = ora('Loading unicorns')
    spinner.start();
    download(
        `direct:https://gitee.com/Wuaixi/wu-vue3-ts-template.git#${branch}`,
        name,
        { clone: true },
         (err) => {
            if(err) {
                spinner.fail('下载失败！')
                return
            }
            spinner.succeed('下载成功！')
        })
}